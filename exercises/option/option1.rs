// option1.rs
// Make me compile! Execute `rustlings hint option1` for hints


// you can modify anything EXCEPT for this function's sig
fn print_number(maybe_number: Option<u16>) {
    println!("printing: {}", maybe_number.unwrap());
}

fn main() {
    fn main() {
        let mut numbers: [Option<u16>; 5] = [Some(0); 5];
        for iter in 0..5 {
            let number_to_add: u16 = 1;
            numbers[iter] = Some(number_to_add);
        }
    }
}
